import React, { useState } from 'react';

function useForm(defaults) {
  const [values, setValues] = useState(defaults);
  function updateValue(e) {
    let { value } = e.target;
    if (e.target.type === 'number') {
      value = parseInt(e.target.value);
    }
    setValues({
      // copy the existing values into the object
      ...values,
      // update the new value that changed
      [e.target.name]: value,
    });
  }
  return { values, updateValue };
}

export default useForm;
