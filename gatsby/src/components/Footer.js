import React from 'react';

function Footer(props) {
  return (
    <footer>
      <p>&copy; Slick's Slice {new Date().getFullYear()}</p>
    </footer>
  );
}

export default Footer;
