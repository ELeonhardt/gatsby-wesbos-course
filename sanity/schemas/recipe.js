import { IoFastFood as icon } from 'react-icons/md';

export default {
  name: 'recipe',
  title: 'Recipe',
  type: 'document',
  icon,
  fields: [
    {
      name: 'name',
      title: 'Name',
      type: 'string',
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'name',
        maxLength: 100,
      },
    },
    {
      name: 'duration',
      title: 'Duration',
      type: 'number',
      description: 'How long does cooking take in minutes?',
    },
    {
      name: 'instrucctions',
      title: 'Instructions',
      type: 'array',
      of: [{ type: 'string' }],
    },
    {
      name: 'ingredients',
      title: 'Ingredients',
      type: 'array',
      of: [{ type: 'ingredientObject' }],
    },
  ],
};
