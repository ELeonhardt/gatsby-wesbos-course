import { FaPepperHot as icon, FaLeaf as leaf } from 'react-icons/fa';

export default {
  name: 'ingredient',
  title: 'Ingredient',
  type: 'document',
  icon: leaf,
  fields: [
    {
      name: 'name',
      title: 'Ingredient Name',
      type: 'string',
      description: 'Name of the ingredient',
    },
    {
      name: 'slug',
      title: 'Slug',
      type: 'slug',
      options: {
        source: 'name',
        maxLength: 100,
      },
    },
    {
      name: 'store',
      title: 'Store',
      type: 'string',
      description: 'In which store to buy the ingredient',
    },
    {
      name: 'unit',
      title: 'Unit',
      type: 'string',
      description:
        'In which unit the ingredient is specified in (kg, grams, cups...)',
    },
  ],
};
