import { FaCheese as icon, FaLeaf as leaf } from 'react-icons/fa';

export default {
  name: 'ingredientObject',
  title: 'Ingredient of Meal',
  type: 'object',
  icon,
  fields: [
    {
      name: 'amount',
      title: 'Amount',
      type: 'number',
      description: 'Amount of the specified units in the recipe',
    },
    {
      name: 'ingredient',
      title: 'Ingredient',
      type: 'reference',
      to: [{ type: 'ingredient' }],
    },
  ],
};
